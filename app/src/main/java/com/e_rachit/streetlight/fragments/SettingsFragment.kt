package com.e_rachit.streetlight.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.e_rachit.streetlight.IFragmentHelper
import com.e_rachit.streetlight.R
import com.e_rachit.streetlight.Utils
import com.e_rachit.streetlight.models.PrefKeys
import kotlinx.android.synthetic.main.fragment_settings.*
import android.content.DialogInterface
import android.content.DialogInterface.BUTTON_NEUTRAL


/**
 * Created by rohitranjan on 15/03/18.
 */
class SettingsFragment : Fragment() {

    private lateinit var iFragmentHelper: IFragmentHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_settings, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iFragmentHelper.setTitle("Settings")
        toggleNotificationBtn.isChecked = Utils.readPreferenceData(activity!!, PrefKeys.NOTIFICATION_SETTINGS, true)
        toggleNotificationBtn.setOnCheckedChangeListener({ buttonView, isChecked ->
            Utils.savePreferenceData(activity!!, PrefKeys.NOTIFICATION_SETTINGS, isChecked)
            if (isChecked) {
                showAlert("Alert","Alert settings has been enabled.")
                Utils.scheduleJob(this.context!!)
            } else {
                showAlert("Alert","Alert settings has been disabled.")
                Utils.cancelJob(this.context!!)
            }
        })
    }

    /**
     * shows alert
     *
     * @param title
     * @param message
     */
    private fun showAlert(title: String, message: String) {
        val alertDialog = AlertDialog.Builder(activity).create()
        alertDialog.setTitle(title)
        alertDialog.setMessage(message)
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        alertDialog.show()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            iFragmentHelper = context as IFragmentHelper
        } catch (e: Exception) {

        }
    }

}