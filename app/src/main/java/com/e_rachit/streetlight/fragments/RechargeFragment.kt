package com.e_rachit.streetlight.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.e_rachit.streetlight.IFragmentHelper
import com.e_rachit.streetlight.R
import com.e_rachit.streetlight.StreetLightsApplication
import com.e_rachit.streetlight.models.Device
import com.e_rachit.streetlight.models.DeviceRechargeRequest
import com.e_rachit.streetlight.models.UserDevicesResponse
import com.e_rachit.streetlight.services.UserServices
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_recharge.*
import okhttp3.ResponseBody


/**
 * Created by rohitranjan on 15/03/18.
 */
class RechargeFragment : Fragment() {

    private lateinit var iFragmentHelper: IFragmentHelper
    private val userServices = StreetLightsApplication.Companion.retrofit!!.create(UserServices::class.java)
    private var deviceMap: Map<String, Device>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_recharge, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iFragmentHelper.setTitle("Recharge")
        fetchDevices()

        recharge_device_btn.setOnClickListener {
            deviceMap?.let {
                AlertDialog.Builder(activity)
                        .setTitle("Recharge Confirmation")
                        .setMessage("Do you want recharge the device \""+deviceMap!!.get(spinner1.selectedItem.toString())!!.slc_id+"\" ?")
                        .setPositiveButton(android.R.string.yes, DialogInterface.OnClickListener { dialog, which ->
                            rechargeDevice(deviceMap!!.get(spinner1.selectedItem.toString())!!.device_id)
                        })
                        .setNegativeButton(android.R.string.no, DialogInterface.OnClickListener { dialog, which ->
                            dialog.dismiss()
                        }).show()
            }

        }
    }


    /**
     * recharges the device with the passed device ID
     *
     * @param deviceId: The device Id which needs to be recharged
     */
    private fun rechargeDevice(deviceId: String) {
        userServices.rechargeDevice(DeviceRechargeRequest(deviceId)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseBody> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: ResponseBody) {
                        Toast.makeText(activity, "Device Recharge Request sent.", Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(e: Throwable) {
                        Toast.makeText(activity, "Unabke to send Device Recharge Request.", Toast.LENGTH_SHORT).show()
                    }

                })
    }

    /**
     * fetches the devices from the server
     */
    private fun fetchDevices() {
        userServices.fetchUserDevices().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<UserDevicesResponse> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: UserDevicesResponse) {
                        deviceMap = t.deviceList.associateBy({ it.location }, { it })
                        //Creating the ArrayAdapter instance having the country list
                        val aa = ArrayAdapter(activity, android.R.layout.simple_spinner_item, t.deviceList.map { it.location })
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        //Setting the ArrayAdapter data on the Spinner
                        spinner1.setAdapter(aa)
                    }

                    override fun onError(e: Throwable) {
                        Toast.makeText(activity, "Unable to fetch User Devices. Please check your internet connection.", Toast.LENGTH_SHORT).show()
                    }

                    override fun onComplete() {

                    }
                })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            iFragmentHelper = context as IFragmentHelper
        } catch (e: Exception) {

        }
    }
}