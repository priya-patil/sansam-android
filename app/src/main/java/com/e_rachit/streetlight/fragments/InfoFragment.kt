package com.e_rachit.streetlight.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.e_rachit.streetlight.IFragmentHelper
import com.e_rachit.streetlight.R

/**
 * Created by rohitranjan on 15/03/18.
 */
class InfoFragment : Fragment() {

    private lateinit var iFragmentHelper: IFragmentHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_info, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iFragmentHelper.setTitle("Info")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            iFragmentHelper = context as IFragmentHelper
        } catch (e: Exception) {

        }
    }

}