package com.e_rachit.streetlight.fragments

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.os.Environment
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.content.FileProvider
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.e_rachit.streetlight.IFragmentHelper
import com.e_rachit.streetlight.R
import com.e_rachit.streetlight.StreetLightsApplication
import com.e_rachit.streetlight.StreetLightsApplication.Companion.baseURL
import com.e_rachit.streetlight.models.*
import com.e_rachit.streetlight.services.DownloadManager
import com.e_rachit.streetlight.services.UserServices
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_reports.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by rohitranjan on 15/03/18.
 */
class ReportsFragment : Fragment() {

    private lateinit var iFragmentHelper: IFragmentHelper
    private lateinit var deviceReportAdapter: DevicesReportAdapter
    private val userServices = StreetLightsApplication.Companion.retrofit!!.create(UserServices::class.java)
    private var deviceMap: Map<String, Device>? = null
    val deviceReports = mutableListOf<DeviceReport>()
    val sdf = SimpleDateFormat("dd-MM-yyyy");
    val sdf1 = SimpleDateFormat("yyyy-MM-dd");

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_reports, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iFragmentHelper.setTitle("Reports")

        /* setup the view pager adapter */
        deviceReportAdapter = DevicesReportAdapter(deviceReports)
        val mLayoutManager = LinearLayoutManager(this@ReportsFragment.context)
        reports_list_rv.setLayoutManager(mLayoutManager)
        reports_list_rv.setItemAnimator(DefaultItemAnimator())
        reports_list_rv.setAdapter(deviceReportAdapter)

        val myCalendar = Calendar.getInstance()
        val startDate = Calendar.getInstance()
        startDate.add(Calendar.DAY_OF_YEAR, -7)

        reports_start_date.setText(sdf.format(startDate.time))
        reports_end_date.setText(sdf.format(myCalendar.time))

        reports_start_date.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    val dp = DatePickerDialog(this@ReportsFragment.context, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                        reports_start_date.setText(sdf.format(sdf.parse(StringBuilder().append(dayOfMonth).append("-").append(month + 1).append("-").append(year).toString())))
                    }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH))
                    dp.datePicker.maxDate = Date().time
                    dp.show()
                    return@setOnTouchListener true
                }
            }
            false
        }
        reports_end_date.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    val dp = DatePickerDialog(this@ReportsFragment.context, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                        reports_end_date.setText(sdf.format(sdf.parse(StringBuilder().append(dayOfMonth).append("-").append(month + 1).append("-").append(year).toString())))
                    }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH))
                    dp.datePicker.maxDate = Date().time
                    dp.show()
                    return@setOnTouchListener true
                }
            }
            false
        }
        report_go_btn.setOnClickListener {
            fetchReports()
        }

        report_download_btn.setOnClickListener {
            if (!(activity as IFragmentHelper).checkForStoragePermission()) {
                return@setOnClickListener
            }

            val deviceId = if (reports_device_selection.visibility == View.GONE) "" else deviceMap!!.get(reports_device_selection.selectedItem.toString())!!.device_id
            val filename = (if (deviceId.isEmpty()) "all-devices" else deviceMap!!.get(reports_device_selection.selectedItem.toString())!!.device_id) + "__" + reports_start_date.text + "--" + reports_end_date.text + "-report-sansam.pdf"
            val basePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).absolutePath + "/Sansam Reports"
            val file = File(basePath, filename)
            if (!File(basePath).exists())
                File(basePath).mkdirs()

            if ((file.exists()))
                file.delete()
            val downloads = mutableListOf<DownloadFile>()
            downloads.add(DownloadFile(baseURL + "/streetlight/users/download_reports?device_id=" + deviceId + "&start_date=" + sdf1.format(sdf.parse(reports_start_date.text.toString()))
                    + "&end_date=" + sdf1.format(sdf.parse(reports_end_date.text.toString())), filename, -1))
            DownloadManager(downloads, basePath, object : DownloadManager.IOnFileDownloadCallback() {
                override fun onBatchDownloadFinished() {
                    super.onBatchDownloadFinished()
                    activity?.runOnUiThread {
                        Toast.makeText(activity, "Report Downloaded", Toast.LENGTH_LONG).show()
                        val mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(".pdf");
                        val intent = Intent()
                        intent.action = android.content.Intent.ACTION_VIEW
                        val uri = FileProvider.getUriForFile(activity!!, activity!!.getApplicationContext().getPackageName() + ".sansam.provider", file)
                        intent.setDataAndType(uri, mime)
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivityForResult(intent, 10)
                    }
                }

                override fun onBatchDownloadFailed() {
                    super.onBatchDownloadFailed()
                    activity?.runOnUiThread {
                        Toast.makeText(activity, "Report Downloaded Failed", Toast.LENGTH_LONG).show()
                    }
                }
            })

        }

        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.all_devices_rb -> {
                    reports_device_selection.visibility = View.GONE
                }
                R.id.indiv_device_rb -> {
                    reports_device_selection.visibility = View.VISIBLE
                }
            }
        }
        fetchDevices()
    }

    /**
     * fetches reports for the selected dates
     */
    fun fetchReports() {
        if (!reports_start_date.text.toString().isEmpty() and !reports_end_date.text.toString().isEmpty()) {
            loading_parent.visibility = View.VISIBLE
            userServices.getReports(
                    if (reports_device_selection.visibility == View.GONE) "" else deviceMap!!.get(reports_device_selection.selectedItem.toString())!!.device_id,
                    sdf1.format(sdf.parse(reports_start_date.text.toString())), sdf1.format(sdf.parse(reports_end_date.text.toString())))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<DeviceReportResponse> {
                        override fun onComplete() {

                        }

                        override fun onSubscribe(d: Disposable) {

                        }

                        override fun onNext(t: DeviceReportResponse) {
                            loading_parent.visibility = View.GONE
                            deviceReports.clear()
                            deviceReports.addAll(t.reportList)
                            deviceReportAdapter.notifyDataSetChanged()
                        }

                        override fun onError(e: Throwable) {
                            loading_parent.visibility = View.GONE
                        }
                    })
        } else {
            Toast.makeText(activity, "Please select the date to continue", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * fetches the devices assigned to the user and refreshes the view with the same
     */
    private fun fetchDevices() {
        userServices.fetchUserDevices().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<UserDevicesResponse> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: UserDevicesResponse) {
                        /* set up the spinner */
                        deviceMap = t.deviceList.associateBy({ it.slc_id }, { it })
                        val list: MutableList<String> = mutableListOf<String>()
//                        list.add(0, "All Devices")
                        list.addAll(t.deviceList.map { it.slc_id }.toSet())
                        val aa = ArrayAdapter(activity, android.R.layout.simple_spinner_item, list)
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        reports_device_selection.setAdapter(aa)
                        fetchReports()
                    }

                    override fun onError(e: Throwable) {
                        activity?.runOnUiThread {
                            Toast.makeText(activity, "Unable to fetch User Devices. Please check your internet connection.", Toast.LENGTH_SHORT).show()
                        }

                    }

                    override fun onComplete() {

                    }
                })
    }


    inner class DevicesReportAdapter(var devicesReports: List<DeviceReport>) : RecyclerView.Adapter<DevicesReportAdapter.DevicesViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DevicesViewHolder =
                DevicesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.device_report_row, parent, false))

        override fun getItemCount(): Int = devicesReports.size

        override fun onBindViewHolder(holder: DevicesViewHolder, position: Int) {
            val deviceReport = devicesReports.get(position)
            holder.createdDate.text = sdf.format(sdf1.parse(deviceReport.created_date))
            holder.deviceSLCId.text = deviceReport.slc_id
            holder.startTime.text = deviceReport.start_time.substring(0, 5)
            holder.endTime.text = deviceReport.end_time.substring(0, 5)
        }


        inner class DevicesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val createdDate: TextView = view.findViewById<View>(R.id.created_date) as TextView
            val startTime: TextView = view.findViewById<View>(R.id.start_time) as TextView
            val endTime: TextView = view.findViewById<View>(R.id.end_time) as TextView
            val deviceSLCId: TextView = view.findViewById(R.id.device_slc_id_value) as TextView
            val deviceReportRowParent: ConstraintLayout = view.findViewById(R.id.device_report_row_parent) as ConstraintLayout

        }

    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            iFragmentHelper = context as IFragmentHelper
        } catch (e: Exception) {

        }
    }

}