package com.e_rachit.streetlight.fragments

import android.app.AlarmManager
import android.app.Dialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.e_rachit.streetlight.IFragmentHelper
import com.e_rachit.streetlight.R
import com.e_rachit.streetlight.StreetLightsApplication
import com.e_rachit.streetlight.Utils
import com.e_rachit.streetlight.models.*
import com.e_rachit.streetlight.receivers.MyAlarmReceiver
import com.e_rachit.streetlight.services.UserServices
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_devices_list.*
import okhttp3.ResponseBody
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by rohitranjan on 15/03/18.
 */
class DevicesListFragment : Fragment() {

    private lateinit var deviceAdapter: DevicesAdapter
    private lateinit var iFragmentHelper: IFragmentHelper
    private var selectedDevices: MutableMap<String, Device>? = null
    private val userServices = StreetLightsApplication.Companion.retrofit!!.create(UserServices::class.java)
    var formatShort = SimpleDateFormat("HH:mm", Locale.US)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_devices_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iFragmentHelper.setTitle("Devices")

        // Create an ArrayAdapter using the string array and a default spinner layout
        val adapter = ArrayAdapter.createFromResource(this.context, R.array.devices_types, android.R.layout.simple_spinner_item)
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        devices_types_spinner.setAdapter(adapter)
        selectedDevices = mutableMapOf()

        val userRole: String = Utils.readPreferenceData(activity, PrefKeys.USER_ROLE, "0")!!
        if (userRole.equals("0")) {
            schedule_btn.visibility = View.GONE
        }

        search_et.clearFocus()
        showDevices(emptyList<Device>())
        devices_types_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                deviceAdapter.filterText(search_et.text.toString(), position)
            }

        }

        switchButton.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked)
                switchButton.setText("Responding")
            else
                switchButton.setText("Not Responding")
            deviceAdapter.filterText(search_et.text.toString(), isChecked)
        }

        fetchDevices()

        search_btn.setOnClickListener {
            deviceAdapter.filterText(search_et.text.toString(), switchButton.isActivated)
        }

        schedule_btn.setOnClickListener {
            selectedDevices?.let {
                if (selectedDevices!!.isNotEmpty()) {
                    showScheduleDialog()
                } else {
                    Toast.makeText(this@DevicesListFragment.context, "Please select some devices to schedule.", Toast.LENGTH_SHORT).show()
                }
            }
        }
        refresh_list_btn.setOnClickListener {
            fetchDevices()
            updateCurrentSchedule()
        }
        switchButton.isActivated = true
        updateCurrentSchedule()
    }


    /**
     * shows a dialog for scheduling the device
     */
    private fun showScheduleDialog() {
        if (selectedDevices!!.isEmpty())
            return

        val dialog = Dialog(this@DevicesListFragment.context)
        dialog.setContentView(R.layout.schedule_dialog)

        dialog.findViewById<EditText>(R.id.schedule_start_time).setOnClickListener {
            val mTimePicker: TimePickerDialog
            mTimePicker = TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                val cal = Calendar.getInstance()
                cal.set(Calendar.HOUR_OF_DAY, selectedHour)
                cal.set(Calendar.MINUTE, selectedMinute)
                dialog.findViewById<EditText>(R.id.schedule_start_time).setText(formatShort.format(cal.time))

            }, 17, 0, true)//Yes 24 hour time
            mTimePicker.setTitle("Select Schedule Start Time")
            mTimePicker.show()
        }

        dialog.findViewById<EditText>(R.id.schedule_end_time).setOnClickListener {
            val mTimePicker: TimePickerDialog
            mTimePicker = TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                val cal = Calendar.getInstance()
                cal.set(Calendar.HOUR_OF_DAY, selectedHour)
                cal.set(Calendar.MINUTE, selectedMinute)

                dialog.findViewById<EditText>(R.id.schedule_end_time).setText(formatShort.format(cal.time))
            }, 5, 0, true)//Yes 24 hour time
            mTimePicker.setTitle("Select Schedule End Time")
            mTimePicker.show()
        }
        dialog.findViewById<Button>(R.id.di_btn_ok).setOnClickListener {
            val a = dialog.findViewById<EditText>(R.id.schedule_start_time).text.toString()
            val b = dialog.findViewById<EditText>(R.id.schedule_end_time).text.toString()
            if (!dialog.findViewById<EditText>(R.id.schedule_start_time).text.trim().isEmpty() && !dialog.findViewById<EditText>(R.id.schedule_end_time).text.trim().isEmpty()) {
                // call the schedule api and dismiss the dialog
                dialog.findViewById<Button>(R.id.di_btn_ok).isEnabled = false
                loading_parent.visibility = View.VISIBLE
                val ss = dialog.findViewById<EditText>(R.id.schedule_end_time).text.toString()
                userServices.scheduleDevices(ScheduleDevicesRequest(dialog.findViewById<EditText>(R.id.schedule_start_time).text.toString(),
                        dialog.findViewById<EditText>(R.id.schedule_end_time).text.toString(), selectedDevices!!.map { x -> x.value.device_id }.joinToString(","))).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(object : Observer<ResponseBody> {
                            override fun onComplete() {

                            }

                            override fun onSubscribe(d: Disposable) {

                            }

                            override fun onNext(t: ResponseBody) {
                                loading_parent.visibility = View.GONE
                                dialog.findViewById<Button>(R.id.di_btn_ok).isEnabled = true
                                dialog.dismiss()
                                Toast.makeText(activity, "Schedule Set", Toast.LENGTH_LONG).show()
                                selectedDevices!!.clear()
                                // refresh the devices
                                fetchDevices()
                                updateCurrentSchedule()
                            }

                            override fun onError(e: Throwable) {
                                loading_parent.visibility = View.GONE
                                Toast.makeText(activity, "Unable to set schedule", Toast.LENGTH_LONG).show()
                                updateCurrentSchedule()
                            }

                        })
            } else {
                Toast.makeText(activity, "Invalid Input", Toast.LENGTH_SHORT).show()
            }

        }
        dialog.show()
        val window = dialog.window
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
    }

    /**
     * shows the list of devices in the recycler view
     *
     * @param devices: the devices to be shown in the list
     */
    private fun showDevices(devices: List<Device>?) {
        devices?.let {
            device_count_tv.text = devices.size.toString()

            // Setup the devices Adapter
            deviceAdapter = DevicesAdapter(devices)
            val mLayoutManager = LinearLayoutManager(this@DevicesListFragment.context)
            devices_list_rv.layoutManager = mLayoutManager
            devices_list_rv.itemAnimator = DefaultItemAnimator()
            devices_list_rv.adapter = deviceAdapter
        }
    }

    /**
     * loads the updated schedule
     */
    private fun updateCurrentSchedule() {
        userServices.fetchCurrentSchedule().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CurrentScheduleResponse> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: CurrentScheduleResponse) {
                        current_schedule_value.setText(t.startTime.substring(0, 5) + " - " + t.endTime.substring(0, 5))
                        Utils.savePreferenceData(activity!!, PrefKeys.CURRENT_SCHEDULE, t.startTime + "-" + t.endTime)

                        /* set the repeating alarm */
                        val alarmManager = activity!!.getSystemService(ALARM_SERVICE) as AlarmManager;
                        val myIntent = Intent(activity, MyAlarmReceiver::class.java)

                        val oldRandId = Utils.readPreferenceData(activity!!, PrefKeys.CURRENT_ALARM_ID, -1);
                        val randId = Random().nextInt((999999 - 100) + 1) + 100
                        Utils.savePreferenceData(activity!!, PrefKeys.CURRENT_ALARM_ID, randId)

                        if (oldRandId >= 100) {
                            val myOldIntent = Intent(activity!!,
                                    MyAlarmReceiver::class.java)
                            val pendingIntent = PendingIntent.getBroadcast(
                                    activity!!, oldRandId, myOldIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                            alarmManager.cancel(pendingIntent)
                        }
                        val newPendingIntent = PendingIntent.getBroadcast(activity, randId, myIntent, 0)

                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Date().time + 60000, 1000 * 60 * 15, newPendingIntent)
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }

                })
    }

    /**
     * fetches the devices assigned to the user and refreshes the view with the same
     */
    private fun fetchDevices() {
        loading_parent.visibility = View.VISIBLE
        userServices.fetchUserDevices().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<UserDevicesResponse> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: UserDevicesResponse) {
                        loading_parent.visibility = View.GONE
                        showDevices(t.deviceList)
                        deviceAdapter.filterText(search_et.text.toString(), switchButton.isActivated)
                    }

                    override fun onError(e: Throwable) {
                        activity?.runOnUiThread {
                            Toast.makeText(activity, "Unable to fetch User Devices. Please check your internet connection.", Toast.LENGTH_SHORT).show()
                            loading_parent.visibility = View.GONE
                        }
                    }

                    override fun onComplete() {

                    }
                })
    }


    inner class DevicesAdapter(var devices: List<Device>) : RecyclerView.Adapter<DevicesAdapter.DevicesViewHolder>() {
        private var filteredDevices: MutableList<Device> = mutableListOf()
        private var isAllChecked = false;

        init {
            filteredDevices.addAll(devices)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DevicesViewHolder =
                DevicesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.device_row, parent, false))

        override fun getItemCount(): Int = filteredDevices.size + 1

        override fun onBindViewHolder(holder: DevicesViewHolder, position: Int) {

            if (position == 0) {
                holder.slcId.text = "Device ID"
                holder.location.text = "Location"
                holder.stateHeading.visibility = View.VISIBLE
                holder.status.visibility = View.GONE

                holder.slcId.setTypeface(null, Typeface.BOLD);
                holder.location.setTypeface(null, Typeface.BOLD);
                holder.stateHeading.setTypeface(null, Typeface.BOLD);

                holder.checkboxSchedule.isChecked = selectedDevices?.size == filteredDevices.size && filteredDevices?.size != 0
                holder.checkboxSchedule.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (!isChecked) {
                        selectedDevices?.clear()
                        isAllChecked = false
                    } else {
                        filteredDevices.map { device -> selectedDevices?.put(device.device_id, device) }
                        isAllChecked = true
                    }
                    try {
                        notifyDataSetChanged()
                    } catch (e: IllegalStateException) {

                    }
                }

            } else {
                val device = filteredDevices[position - 1]


                holder.slcId.setTypeface(null, Typeface.NORMAL);
                holder.location.setTypeface(null, Typeface.NORMAL);
                holder.stateHeading.setTypeface(null, Typeface.NORMAL);

                holder.stateHeading.visibility = View.GONE
                holder.status.visibility = View.VISIBLE
                holder.slcId.text = device.slc_id
                holder.location.text = device.location

                if (device.status == 2)
                    holder.status.setBackgroundResource(R.drawable.switch_off)
                else if (device.status == 1)
                    holder.status.setBackgroundResource(R.drawable.switch_on)
                else
                    holder.status.setBackgroundResource(R.drawable.not_responding)

                holder.checkboxSchedule.isChecked = selectedDevices!!.contains(device.device_id)

                holder.checkboxSchedule.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (!isChecked)
                        selectedDevices?.remove(device.device_id)
                    else
                        selectedDevices?.put(device.device_id, device)
                }
                holder.deviceRowParent.setOnClickListener {
                    showDeviceInfoDialog(device)
                }
            }
        }

        /**
         * show Device Info Dialog
         *
         * @param device: The device for which the info has to be shown
         */
        private fun showDeviceInfoDialog(device: Device) {

            val dialog = Dialog(this@DevicesListFragment.context)
            dialog.setContentView(R.layout.device_info_dialog)

            dialog.findViewById<TextView>(R.id.di_device_schedule_value).text = StringBuilder()
                    .append(if (device.start_time == null) "_" else device.start_time.substring(0, 5))
                    .append(" - ")
                    .append(if (device.end_time == null) "_" else device.end_time.substring(0, 5))
            dialog.findViewById<TextView>(R.id.di_device_slcid_value).text = device.slc_id
            dialog.findViewById<TextView>(R.id.di_device_location_value).text = device.location
            when (device.status) {
                0 -> dialog.findViewById<TextView>(R.id.di_device_status_value).text = "Non Responding"
                1 -> dialog.findViewById<TextView>(R.id.di_device_status_value).text = "Off"
                2 -> dialog.findViewById<TextView>(R.id.di_device_status_value).text = "On"

            }
            dialog.findViewById<TextView>(R.id.di_device_load_value).text = device.device_load
            dialog.findViewById<TextView>(R.id.di_device_meter_reading_value).text = device.meter_reading
            dialog.findViewById<Button>(R.id.di_btn_ok).setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }


        /**
         * filters the text
         *
         * @param text
         */
        fun filterText(text: String, isResponding: Boolean) {
            filteredDevices.clear()
            if (text.isEmpty()) {
//                filteredDevices.addAll(devices)
                filteredDevices.addAll(devices.filter { device -> if (isResponding) device.status != 0 else device.status == 0 }.groupBy { it.location }.flatMap { it.value })
            } else {
                filteredDevices.addAll(devices.filter { device -> device.slc_id.contains(text, true) && if (isResponding) device.status != 0 else device.status == 0 }.groupBy { it.location }.flatMap { it.value })
            }
//            filteredDevices = filteredDevices.groupBy { it.location }.flatMap { it.value }.toMutableList()
//            filteredDevices.addAll(filteredDevices.filter { device -> if (state == 0) device.status == state else device.status != state })
            notifyDataSetChanged()
        }

        inner class DevicesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val slcId: TextView = view.findViewById<View>(R.id.slc_id_value) as TextView
            val location: TextView = view.findViewById(R.id.location_value) as TextView
            val status: View = view.findViewById(R.id.device_status_icon)
            val stateHeading: TextView = view.findViewById(R.id.state_heading);
            val checkboxSchedule: CheckBox = view.findViewById(R.id.checkbox_schedule) as CheckBox
            val deviceRowParent: ConstraintLayout = view.findViewById(R.id.device_row_parent) as ConstraintLayout

        }

    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            iFragmentHelper = context as IFragmentHelper
        } catch (e: Exception) {

        }
    }
}