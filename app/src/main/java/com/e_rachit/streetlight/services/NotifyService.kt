package com.e_rachit.streetlight.services

import android.app.job.JobParameters
import android.app.job.JobService
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import com.e_rachit.streetlight.HomeActivity
import com.e_rachit.streetlight.R
import android.support.v4.app.NotificationManagerCompat
import com.e_rachit.streetlight.Utils
import android.media.RingtoneManager
import com.e_rachit.streetlight.models.PrefKeys
import java.util.*


class NotifyService : JobService() {
    override fun onStartJob(params: JobParameters?): Boolean {
        processNotification(this)
        jobFinished(params, false)
        return false
    }

    override fun onStopJob(params: JobParameters?): Boolean = false


    companion object {

        /**
         * process the notification
         *
         * @param mContext
         */
        fun processNotification(mContext: Context) {
            if (Utils.readPreferenceData(mContext, PrefKeys.NOTIFICATION_SETTINGS, true)) {
                val lastTime = Date(Utils.readPreferenceData(mContext, PrefKeys.LAST_NOTIFY_EXECUTION_TIME, 0L))
                val currentSchedule: String? = Utils.readPreferenceData(mContext, PrefKeys.CURRENT_SCHEDULE, "")
                currentSchedule?.let {
                    if (currentSchedule.isNotEmpty()) {
                        val startTime = Utils.parseTime(currentSchedule.split("-")[0])
                        val endTime = Utils.parseTime(currentSchedule.split("-")[1])
                        val currentTime = Date()
                        if (startTime.after(lastTime) && startTime.before(currentTime)) {
                            showNotification(mContext)
                        } else if (endTime.after(lastTime) && endTime.before(currentTime)) {
                            showNotification(mContext)
                        } else {
//                        showNotification(mContext, "Execution Notification", "Test Notification to proove that the task runs in a periodic interval")
                        }
                    }
                }
                Utils.savePreferenceData(mContext, PrefKeys.LAST_NOTIFY_EXECUTION_TIME, Date().time)

            }
        }

        /**
         * shows local notification
         *
         * @param context
         * @param title
         * @param message
         */
        fun showNotification(context: Context, title: String = "Status Alert", message: String = "Check the updated streetlight status") {
            val intent = Intent(context, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

            val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            val mBuilder = NotificationCompat.Builder(context, "SamSam")
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSound(uri)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
            val notificationManager = NotificationManagerCompat.from(context)
            notificationManager.notify(Utils.rand(0, 9999999), mBuilder.build());
        }

    }

}