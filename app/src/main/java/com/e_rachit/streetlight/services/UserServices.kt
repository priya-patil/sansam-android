package com.e_rachit.streetlight.services

import com.e_rachit.streetlight.models.*
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by rohitranjan on 24/04/16.
 */
interface UserServices {

    @POST("/streetlight/users/user_login")
    fun signIn(@Body userLoginRequest: LoginRequest): Observable<LoginResponse>

    @GET("/streetlight/devices/get_devices_by_userId")
    fun fetchUserDevices(): Observable<UserDevicesResponse>

    @POST("/streetlight/devices/recharge")
    fun rechargeDevice(@Body deviceRechargeRequest: DeviceRechargeRequest): Observable<ResponseBody>

    @POST("/streetlight/devices/set_schedule")
    fun scheduleDevices(@Body scheduleDevicesRequest: ScheduleDevicesRequest): Observable<ResponseBody>

    @GET("/streetlight/users/get_reports")
    fun getReports(@Query("device_id") deviceId: String, @Query("start_date") startDate: String, @Query("end_date") endDate: String): Observable<DeviceReportResponse>

    @GET("/streetlight/devices/getCurrentSchedule")
    fun fetchCurrentSchedule():Observable<CurrentScheduleResponse>

    @Streaming
    @GET
    fun downloadFile(@Url fileUrl: String): Call<ResponseBody>
}
