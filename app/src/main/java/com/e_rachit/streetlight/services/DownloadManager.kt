package com.e_rachit.streetlight.services

import android.os.AsyncTask
import android.os.Handler
import android.util.Log
import com.e_rachit.streetlight.StreetLightsApplication
import com.e_rachit.streetlight.Utils
import com.e_rachit.streetlight.models.DownloadFile
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.*

/**
 * Created by rohitranjan on 06/11/17.
 */
class DownloadManager(val downloadFiles: List<DownloadFile>, var basePath: String, val iOnFileDownloadCallback: DownloadManager.IOnFileDownloadCallback) {

    private val cloudServices = StreetLightsApplication.Companion.retrofit!!.create(UserServices::class.java)
    private var currentDownloadFileIndex = 0
    private val maxDownloadAttempt = 3
    private var filesDownloaded = 0

    init {
        if (downloadFiles.size > 0)
            downloadItem(currentDownloadFileIndex, 0)
    }

    /**
     * download the file with the given index
     *
     * @param index
     * @param attempt
     */
    private fun downloadItem(index: Int, attempt: Int = 0) {
        if (currentDownloadFileIndex == downloadFiles.size) {

            if (filesDownloaded == downloadFiles.size)
                iOnFileDownloadCallback.onBatchDownloadFinished()
            else
                iOnFileDownloadCallback.onBatchDownloadFailed()

        } else {
            downloadFile(downloadFiles[index], attempt)
        }
    }

    /**
     * initiates the download file call
     *
     * @param downloadFile
     * @param attempt
     */
    private fun downloadFile(downloadFile: DownloadFile, attempt: Int) {
        Log.d(DownloadManager::class.java.name, "Downloading file : " + downloadFile.url)
        val filePath = basePath + File.separator + downloadFile.filename
        if (attempt == maxDownloadAttempt) {
            Log.d(DownloadManager::class.java.name, "Retrying file download  : " + downloadFile.url + ", Attempt : " + attempt)
            iOnFileDownloadCallback.onFileDownloadFailed(currentDownloadFileIndex, attempt, downloadFile)
            currentDownloadFileIndex += 1
            downloadItem(currentDownloadFileIndex, 0)
        } /*else if (downloadFile.contentLength == File(filePath).length()) {
            Log.d(DownloadManager::class.java.name, "Downloaded file : " + downloadFile.url)
            filesDownloaded += 1
            currentDownloadFileIndex += 1
            downloadItem(currentDownloadFileIndex, 0)
        }*/ else {

            val isInternetConnected = Utils.InternetCheckTask().execute(StreetLightsApplication.application).get()
            if (isInternetConnected)
                cloudServices.downloadFile(downloadFile.url).enqueue(object : retrofit2.Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        reDownloadItem(downloadFile, attempt, "Response from the server has failed")
                    }

                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        response?.let {
                            DownloadFileAsyncTask(downloadFile, filePath, attempt, response).execute()
                        }
                    }

                })
            else {
                downloadItem(currentDownloadFileIndex, maxDownloadAttempt)
            }
        }
    }

    /**
     * redownload the item
     *
     * @param downloadFile
     * @param attempt
     * @param message
     */
    private fun reDownloadItem(downloadFile: DownloadFile, attempt: Int, message: String) {
        Handler().postDelayed({
            iOnFileDownloadCallback.onRetryingFileDownload(currentDownloadFileIndex, attempt, downloadFile)
            downloadItem(currentDownloadFileIndex, attempt + 1)
        }, 2000)
    }

    /**
     * The main Download File Task
     * Note:- I moved into the async task because of the @Streaming annotation on the download file api by retrofit which makes the response in the main thread hence NetworkOnMainThreadException
     *
     * @param downloadFile
     * @param filePath
     * @param attempt
     * @param response
     *
     */
    inner class DownloadFileAsyncTask(val downloadFile: DownloadFile, val filePath: String, val attempt: Int, val response: Response<ResponseBody>) : AsyncTask<Void, Void, Boolean>() {
        override fun doInBackground(vararg params: Void?): Boolean {
            if (response.isSuccessful) {
                if (!(File(filePath).exists())) {
                    if (File(filePath).exists())
                        File(filePath).delete()

                    val writtenToDisk: Boolean = writeResponseBodyToDisk(filePath, response.body()!!)
                    if (writtenToDisk) {
                        iOnFileDownloadCallback.onFileDownloaded(currentDownloadFileIndex, attempt, downloadFile)
                        Log.d(DownloadManager::class.java.name, "Downloaded file : " + downloadFile.url)
                        return true
                    }
                    return false
                } else {
                    Log.d(DownloadManager::class.java.name, "Downloaded file : " + downloadFile.url)

                    return true
                }
            } else {
                return false
            }
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            if (result == true) {
                currentDownloadFileIndex += 1
                filesDownloaded += 1
                downloadItem(currentDownloadFileIndex)
            } else {
                reDownloadItem(downloadFile, attempt, "Unable to write the file to disk")
            }
        }


        /**
         * writes the response to the disk
         *
         * @param path: the path where the file has to be saved
         * @param body: the content that needs to be saved
         */
        private fun writeResponseBodyToDisk(path: String, body: ResponseBody): Boolean {
            try {
                val futureStudioIconFile = File(path)

                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null

                try {
                    val fileReader = ByteArray(4096)

                    val fileSize = body.contentLength()
                    var fileSizeDownloaded: Long = 0

                    inputStream = body.byteStream()
                    outputStream = FileOutputStream(futureStudioIconFile)
                    Log.d(DownloadManager::class.java.name, "Downloading file started.")
                    while (true) {
                        val read = inputStream!!.read(fileReader)

                        if (read == -1) {
                            break
                        }

                        outputStream.write(fileReader, 0, read)

                        fileSizeDownloaded += read.toLong()
                        // Log.d(DownloadManager::class.java.name, "File Downloaded Size : " + fileSizeDownloaded.toString())
                    }

                    outputStream.flush()
                    Log.d(DownloadManager::class.java.name, "File Downloaded")
                    return true
                } catch (e: IOException) {
                    return false
                } finally {
                    if (inputStream != null) {
                        inputStream.close()
                    }

                    if (outputStream != null) {
                        outputStream.close()
                    }
                }
            } catch (e: IOException) {
                return false
            }

        }
    }


    abstract class IOnFileDownloadCallback {
        open fun onFileDownloaded(index: Int, attempt: Int, downloadFile: DownloadFile? = null) {}
        open fun onFileDownloadFailed(index: Int, attempt: Int, downloadFile: DownloadFile? = null) {}
        open fun onRetryingFileDownload(index: Int, attempt: Int, downloadFile: DownloadFile? = null) {}
        open fun onBatchDownloadFinished() {}
        open fun onBatchDownloadFailed() {}
    }
}