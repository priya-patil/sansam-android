package com.e_rachit.streetlight

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.AsyncTask
import android.os.Build
import android.preference.PreferenceManager
import android.util.Log
import com.e_rachit.streetlight.StreetLightsApplication.Companion.baseURL
import com.e_rachit.streetlight.services.NotifyService
import com.google.gson.Gson
import okio.Buffer
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

object Utils {


    var gson = Gson()


    val random = Random()

    /**
     * delete specific keys value pair from the preferences
     *
     * @param context
     * @param key
     */
    fun deletePreferenceData(context: Context, key: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sp.edit()
        editor.remove(key)
        editor.apply()
    }

    /**
     * read user preferences
     *
     * @param context
     * @param key
     * @param defaultValue
     * @return
     */
    fun readPreferenceData(context: Context?, key: String, defaultValue: String): String? {
        if (context != null) {
            val sp = PreferenceManager.getDefaultSharedPreferences(context)
            return sp.getString(key, defaultValue)
        }
        return null
    }

    /**
     * parses the time in the format "HH:MM:SS"
     *
     * @param time
     */
    fun parseTime(time: String): Date {
        val timeSplit = time.split(":")
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, timeSplit[0].toInt())
        cal.set(Calendar.MINUTE, timeSplit[1].toInt())
        return cal.time
    }

    /**
     * read user preferences
     *
     * @param context
     * @param key
     * @param defaultValue
     * @return
     */
    fun readPreferenceData(context: Context?, key: String, defaultValue: Long): Long {
        if (context != null) {
            val sp = PreferenceManager.getDefaultSharedPreferences(context)
            return sp.getLong(key, defaultValue)
        }
        return 0
    }

    /**
     * read user preferences
     *
     * @param context
     * @param key
     * @param defaultValue
     * @return
     */
    fun readPreferenceData(context: Context?, key: String, defaultValue: Boolean): Boolean {
        if (context != null) {
            val sp = PreferenceManager.getDefaultSharedPreferences(context)
            return sp.getBoolean(key, defaultValue)
        }
        return false
    }

    /**
     * read user preferences
     *
     * @param context
     * @param key
     * @param defaultValue
     * @return
     */
    fun readPreferenceData(context: Context?, key: String, defaultValue: Int): Int {
        if (context != null) {
            val sp = PreferenceManager.getDefaultSharedPreferences(context)
            return sp.getInt(key, defaultValue)
        }
        return -1
    }

    /**
     * overriding the function to add the gson conversion to it
     *
     * @param context
     * @param key
     * @param defaultValue
     * @param clazz
     * @return
     */
    fun readPreferenceData(context: Context, key: String, defaultValue: String, clazz: Class<*>): Any? {
        val result = readPreferenceData(context, key, defaultValue)
        return if (result != null) {
            gson.fromJson(result, clazz)
        } else result
    }

    /**
     * save user preferences
     *
     * @param context
     * @param key
     * @param value
     */
    fun savePreferenceData(context: Context, key: String, value: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sp.edit()
        editor.putString(key, value)
        editor.apply()
    }

    /**
     * save user preferences
     *
     * @param context
     * @param key
     * @param value
     */
    fun savePreferenceData(context: Context, key: String, value: Long) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sp.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    /**
     * save user preferences
     *
     * @param context
     * @param key
     * @param value
     */
    fun savePreferenceData(context: Context, key: String, value: Boolean?) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sp.edit()
        editor.putBoolean(key, value!!)
        editor.apply()
    }

    /**
     * save user preferences
     *
     * @param context
     * @param key
     * @param value
     */
    fun savePreferenceData(context: Context, key: String, value: Int) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sp.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    /**
     * clear all the key value pairs from the preferences
     *
     * @param context
     */
    fun clearPreferences(context: Context) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sp.edit()
        editor.clear()
        editor.apply()
    }

    /**
     * removes the passed in key from the preferences
     *
     * @param context
     * @param key
     */
    fun removePreferenceData(context: Context, key: String) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().remove(key).apply()
    }


    /**
     * Returns `s` with control characters and non-ASCII characters replaced with '?'.
     */
    fun toHumanReadableAscii(s: String): String {
        var i = 0
        val length = s.length
        var c: Int
        while (i < length) {
            c = s.codePointAt(i)
            if (c > '\u001f'.toInt() && c < '\u007f'.toInt()) {
                i += Character.charCount(c)
                continue
            }

            val buffer = Buffer()
            buffer.writeUtf8(s, 0, i)
            var j = i
            while (j < length) {
                c = s.codePointAt(j)
                buffer.writeUtf8CodePoint(if (c > '\u001f'.toInt() && c < '\u007f'.toInt()) c else '?'.toInt())
                j += Character.charCount(c)
            }
            return buffer.readUtf8()
            i += Character.charCount(c)
        }
        return s
    }

    /**
     * schedules the job
     *
     * @param context
     */
    fun scheduleJob(context: Context) {
        val serviceComponent = ComponentName(context, NotifyService::class.java)
        val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            JobInfo.Builder(2398465, serviceComponent)
                    .setPeriodic(10 * 60 * 1000)
                    .setTriggerContentMaxDelay(2000)
        } else {
            JobInfo.Builder(2398465, serviceComponent)
                    .setPeriodic(10 * 60 * 1000)
        }
        val jobScheduler = context.getSystemService(JobScheduler::class.java)
        jobScheduler!!.schedule(builder.build())
    }

    /**
     * cancels the Job Scheduler
     *
     * @param context
     */
    fun cancelJob(context: Context) {
        val jobScheduler = context.getSystemService(JobScheduler::class.java)
        jobScheduler!!.cancel(2398465)
    }


    /**
     * create Notification Channel
     *
     * @param context
     */
    fun createNotificationChannel(context: Context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "SamSam"
            val description = "Beep"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel("SamSam", name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = context.getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    /**
     * generates random in the range
     *
     * @param from
     * @param to
     */
    fun rand(from: Int, to: Int): Int {
        return random.nextInt(to - from) + from
    }

    open class InternetCheckTask : AsyncTask<Context, Void, Boolean>() {

        override fun doInBackground(vararg params: Context): Boolean? {
            val context = params[0]
            return hasInternetAccess(context)
        }

        private fun hasInternetAccess(context: Context): Boolean {
            try {
                val urlc = URL(baseURL)
                        .openConnection() as HttpURLConnection
                urlc.connect()
                Log.e("TAG", "Internet available!")
                return urlc.responseCode / 100 == 2
            } catch (e: IOException) {
                Log.e("TAG", "Error checking internet connection", e)
            }

            return false
        }
    }
}