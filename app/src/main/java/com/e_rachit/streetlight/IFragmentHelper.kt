package com.e_rachit.streetlight

/**
 * Created by rohitranjan on 15/03/18.
 */
interface IFragmentHelper {
    fun setTitle(title: String)
    fun checkForStoragePermission(): Boolean
}