package com.e_rachit.streetlight

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.WindowManager
import android.widget.Toast
import com.e_rachit.streetlight.models.LoginRequest
import com.e_rachit.streetlight.models.LoginResponse
import com.e_rachit.streetlight.models.PrefKeys
import com.e_rachit.streetlight.services.UserServices
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : Activity() {

    private val userServices = StreetLightsApplication.Companion.retrofit!!.create(UserServices::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val window = window

        StreetLightsApplication.currentActivity = this

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary))

        login_btn.setOnClickListener {
            /*if (phone_number_input.text.length != 10) {
                Toast.makeText(this@LoginActivity, "Invalid Phone Number", Toast.LENGTH_SHORT).show()
            } else if (password_input.text.isEmpty()) {
                Toast.makeText(this@LoginActivity, "Invalid Password", Toast.LENGTH_SHORT).show()
            } else {*/
            userServices.signIn(LoginRequest(phone_number_input.text.toString(), password_input.text.toString()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<LoginResponse> {
                        override fun onSubscribe(d: Disposable) {
                        }

                        override fun onNext(value: LoginResponse) {
                            value.let {
                                if (value.token == null) {
                                    Toast.makeText(this@LoginActivity, "Invalid Credentials", Toast.LENGTH_LONG).show()
                                    return
                                }

                                Utils.savePreferenceData(this@LoginActivity, PrefKeys.USER_TOKEN, value.token)
                                Utils.savePreferenceData(this@LoginActivity, PrefKeys.FIRST_NAME, value.firstName)
                                Utils.savePreferenceData(this@LoginActivity, PrefKeys.LAST_NAME, value.lastName)
                                Utils.savePreferenceData(this@LoginActivity, PrefKeys.USER_ROLE, value.role)
                                val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                                intent.flags = intent.getFlags() or Intent.FLAG_ACTIVITY_NO_HISTORY
                                startActivity(intent)
                            }
                        }

                        override fun onError(e: Throwable) {
                            Toast.makeText(this@LoginActivity, "Invalid Credentials", Toast.LENGTH_SHORT).show()
                        }

                        override fun onComplete() {

                        }

                    })
        }
    }
}
