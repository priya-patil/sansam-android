package com.e_rachit.streetlight.models

data class LoginRequest(val phonenumber: String, val password: String)