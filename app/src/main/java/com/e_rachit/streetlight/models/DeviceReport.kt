package com.e_rachit.streetlight.models

/**
 * Created by rohitranjan on 15/03/18.
 */
data class DeviceReport(val device_id: String, val slc_id: String, val created_date: String, val start_time: String, val end_time: String)

data class DeviceReportResponse(val reportList: List<DeviceReport>)