package com.e_rachit.streetlight.models

data class DownloadFile(val url: String, val filename: String, val contentLength: Long)