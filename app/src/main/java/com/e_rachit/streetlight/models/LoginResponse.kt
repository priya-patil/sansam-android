package com.e_rachit.streetlight.models

data class LoginResponse(val token: String?, val firstName: String, val role: String, val lastName: String)