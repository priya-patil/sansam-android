package com.e_rachit.streetlight.models


data class Device(val device_id: String, val slc_id: String, val location: String, val status: Int, val account_id: String, val meter_reading: String, val device_load: String, val start_time: String?, val end_time: String?)
