package com.e_rachit.streetlight.models

object PrefKeys {
    const val USER_TOKEN: String = "USER_TOKEN"
    const val LAST_NAME: String = "LAST_NAME"
    const val FIRST_NAME: String = "FIRST_NAME"
    const val USER_ROLE: String = "USER_ROLE"
    const val CURRENT_SCHEDULE: String = "CURRENT_SCHEDULE"
    const val CURRENT_ALARM_ID: String = "CURRENT_ALARM_ID"
    const val LAST_NOTIFY_EXECUTION_TIME: String = "LAST_NOTIFY_EXECUTION_TIME"
    const val NOTIFICATION_SETTINGS: String = "NOTIFICATION_SETTINGS"
}