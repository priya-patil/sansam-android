package com.e_rachit.streetlight.models

data class ScheduleDevicesRequest(val start_time: String, val end_time: String, val device_id: String)