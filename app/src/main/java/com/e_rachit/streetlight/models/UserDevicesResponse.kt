package com.e_rachit.streetlight.models

data class UserDevicesResponse(val deviceList: List<Device>)