package com.e_rachit.streetlight.models

data class CurrentScheduleResponse(val startTime: String, val endTime: String)