package com.e_rachit.streetlight.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.e_rachit.streetlight.Utils

class AppBootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Utils.scheduleJob(context!!)
    }

}