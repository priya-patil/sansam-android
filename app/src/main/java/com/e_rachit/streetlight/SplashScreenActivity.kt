package com.e_rachit.streetlight

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.e_rachit.streetlight.models.PrefKeys

class SplashScreenActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        StreetLightsApplication.currentActivity = this

        Handler().postDelayed({
            val intent: Intent
            if (Utils.readPreferenceData(this@SplashScreenActivity, PrefKeys.USER_TOKEN, "")!!.isEmpty()) {
                intent = Intent(this@SplashScreenActivity, LoginActivity::class.java)
            } else {
                intent = Intent(this@SplashScreenActivity, HomeActivity::class.java)
            }
            intent.flags = intent.getFlags() or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
//            finish()

        }, 2000)
    }
}
