package com.e_rachit.streetlight

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import com.e_rachit.streetlight.models.PrefKeys
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric



/**
 * Created by rohitranjan on 14/03/18.
 */
class StreetLightsApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        application = this
        Fabric.with(this, Crashlytics())
        init()
    }

    fun init() {
        val okHttpClient = OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(Interceptor { chain ->
                    val original = chain.request()
                    val builder = original.newBuilder()
                    val token = Utils.readPreferenceData(applicationContext, PrefKeys.USER_TOKEN, "")
                    if (!token!!.isEmpty()) {
                        builder.header("Authorization", token)
                                .header("User-Agent", "Android")
                                .header("APP_VERSION", APP_VERSION_VALUE!!.toString())
                        /*for (key in original.headers().names()) {
                            builder.header(key, Utils.toHumanReadableAscii(original.header(key)))
                        }*/
                    }
                    return@Interceptor chain.proceed(builder
                            .method(original.method(), original.body())
                            .build())
                })
                .addInterceptor { chain ->
                    val response = chain.proceed(chain.request())
                    val unAuthorized = response.code() == 401
                    if (unAuthorized) {
                        Utils.clearPreferences(applicationContext)
                        currentActivity?.let{
                            val intentForRegistration = Intent(applicationContext, SplashScreenActivity::class.java)
                            intentForRegistration.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            applicationContext.startActivity(intentForRegistration)
                            currentActivity!!.finish()
                        }
                    }
                    response
                }
//                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()

        retrofit = Retrofit.Builder()
                .baseUrl(baseURL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
    }

    companion object {
        var currentActivity: Activity? = null
        val baseURL = "http://www.e-rachit.com"
        var retrofit: Retrofit? = null
        var application: Context? = null
        var APP_VERSION_VALUE: Int? = BuildConfig.VERSION_CODE
    }
}