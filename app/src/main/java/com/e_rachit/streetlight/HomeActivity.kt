package com.e_rachit.streetlight

import android.Manifest
import android.app.AlarmManager
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.e_rachit.streetlight.fragments.*
import com.e_rachit.streetlight.models.PrefKeys
import kotlinx.android.synthetic.main.activity_main.*
import android.app.PendingIntent
import com.e_rachit.streetlight.receivers.MyAlarmReceiver
import com.e_rachit.streetlight.services.NotifyService
import java.util.*


class HomeActivity : AppCompatActivity(), View.OnClickListener, IFragmentHelper {

    private lateinit var menuItems: List<LinearLayout>
    private val REQUEST_PHONE_CALL = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        StreetLightsApplication.currentActivity = this
        initDrawer()

        /* check if the user is logged in or not*/
        checkLoggedInStatus()

        /* set the first Fragment */
        replaceFragment(DevicesListFragment())

        checkForStoragePermission()
        Utils.createNotificationChannel(HomeActivity@ this)
        if (Utils.readPreferenceData(this, PrefKeys.NOTIFICATION_SETTINGS, true)) {
//            Utils.scheduleJob(HomeActivity@ this)
        }
//        NotifyService.showNotification(this)
        /*val alarmManager = getSystemService (ALARM_SERVICE) as AlarmManager;
        val myIntent = Intent(this@HomeActivity, MyAlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this@HomeActivity, 0, myIntent, 0)
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 18)
        calendar.set(Calendar.MINUTE, 30)
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent)*/
    }

    /**
     * checks for the storage Permission
     *
     * @return Boolean
     */
    public override fun checkForStoragePermission(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        }
        return true
    }

    /**
     * checks if the user is logged in or not
     */
    private fun checkLoggedInStatus() {
        if (Utils.readPreferenceData(this@HomeActivity, PrefKeys.USER_TOKEN, "")!!.isEmpty()) {
            val intent = Intent(this@HomeActivity, LoginActivity::class.java)
            intent.flags = intent.getFlags() or Intent.FLAG_ACTIVITY_NO_HISTORY
            startActivity(intent)
        }
    }

    /**
     * initializes the Side Drawer
     */
    private fun initDrawer() {
        nav_view.setNavigationItemSelectedListener(object : NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                item.isChecked = true
                drawer_layout.closeDrawers()
                return true
            }
        })
        toolbar.title = "Street Light"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_action_menu)
        user_id.text = StringBuffer().append(Utils.readPreferenceData(this@HomeActivity, PrefKeys.FIRST_NAME, ""))
                .append(" ")
                .append(Utils.readPreferenceData(this@HomeActivity, PrefKeys.LAST_NAME, ""))
        menuItems = listOf(devices_menu_btn, recharge_menu_btn, reports_menu_btn, info_menu_btn, info_contact_btn, info_settings_btn, logout_menu_btn)
        menuItems.forEach { v -> v.setOnClickListener(this@HomeActivity) }

    }

    /**
     * sets the title of the fragment
     *
     * @param title: The title to be set
     */
    override fun setTitle(title: String) {
        toolbar?.title = title
    }

    /**
     * replace fragment with the other fragment
     *
     * @param fragment
     */
    fun replaceFragment(fragment: Fragment, addToBackStack: Boolean = true, pickFromBackstack: Boolean = false) {
        try {
            val backStateName = fragment.javaClass.name
            val manager = supportFragmentManager
            manager.executePendingTransactions()

            /* we are using the existing fragment here to prevent it to be added to the backstack if teh user presses it twice */
            val existingFrag = manager.findFragmentById(R.id.frag_container)

            /* we are using this stackFragment to check if the fargment instance is already in the stack so
            * rather than creating a new instance we coulc reuse this same old one*/
            val stackFragment = manager.findFragmentByTag(backStateName)
            if (stackFragment != null && existingFrag != null && existingFrag.javaClass.canonicalName == fragment.javaClass.canonicalName && existingFrag == stackFragment) {
                manager.beginTransaction().detach(existingFrag).attach(existingFrag).commitNow()
                return
            }
            if (!this.isFinishing) {
                val ft = manager.beginTransaction()
                ft.replace(R.id.frag_container, if (pickFromBackstack) stackFragment
                        ?: fragment else fragment, backStateName)

                if (addToBackStack && !fragment.isAdded) {
                    ft.addToBackStack(backStateName)
                    ft.commit()
                } else
                    ft.commitNow()
            }
        } catch (e: Exception) {
            //TODO - handle it
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                REQUEST_PHONE_CALL -> {
                    callAdmin()
                }
            }
        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View) {
        selectMenuItem(v)

        when (v.id) {
            R.id.devices_menu_btn -> {
                replaceFragment(DevicesListFragment())
            }
            R.id.recharge_menu_btn -> {
                replaceFragment(RechargeFragment())
            }
            R.id.reports_menu_btn -> {
                replaceFragment(ReportsFragment())
            }
            R.id.info_settings_btn -> {
                replaceFragment(SettingsFragment())
            }
            R.id.info_menu_btn -> {
                replaceFragment(InfoFragment())
            }
            R.id.info_contact_btn -> {
                callAdmin()
            }
            R.id.logout_menu_btn -> {
                Toast.makeText(this, "Logging Out", Toast.LENGTH_SHORT).show()
                Utils.clearPreferences(this@HomeActivity)
                val intent = Intent(this@HomeActivity, LoginActivity::class.java)
                intent.flags = intent.getFlags() or Intent.FLAG_ACTIVITY_NO_HISTORY
                startActivity(intent)
            }
        }

        drawer_layout.closeDrawers()
    }

    /**
     * calls the admin of the app
     */
    private fun callAdmin() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+91-8105664987"))
        if (ContextCompat.checkSelfPermission(HomeActivity@ this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity@ this, arrayOf(Manifest.permission.CALL_PHONE), REQUEST_PHONE_CALL)
        } else {
            startActivity(intent);
        }
    }


    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) run {
            HomeActivity@ this.moveTaskToBack(true)
        }
        else
            super.onBackPressed()

    }

    private fun selectMenuItem(v: View) {
        menuItems.forEach { view -> view.background = null }
        v.background = getDrawable(R.drawable.menu_item_selected)
    }

    interface IFragmentHelper {
        fun setTitle(title: String)
    }

}
